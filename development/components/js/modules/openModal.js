	module.exports = class AddOpenModal {
		constructor(enter, exit) {
			this.enter = enter;
			this.exit = exit;
		}

		static info() {
			console.log('MODULE:', this.name, true);
		}

		run() {
			const elem = document.querySelector(`${this.enter}`);
			// const output = document.querySelector(`${this.exit}`);
			if (elem) {
				this.constructor.info();

				// const openModal = () => {
				// 	const btn = document.querySelector('.js__button');
				// 	const modal = document.querySelector('.js__modal');
				// 	btn.addEventListener('click', () => {
				// 		modal.classList.toggle('open');
				// 	});
				// };
				// openModal();
			}
		}
	};
