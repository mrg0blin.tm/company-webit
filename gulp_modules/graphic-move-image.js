/* ==== IMPORT PARAMS ==== */
'use strict';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inDevApps = `${inDev}/components`,
inPub = 'public';
/* ==== ----- ==== */


module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
		src(`${inDevApps}/img/**/*.{jpg,jpeg,png,gif}`),
		_run.if(isPublic, _run.filesize()),
		dest(`${inPub}/media/img`)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

