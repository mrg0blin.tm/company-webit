/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"connect": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./development/components/js/connect.js","plugins"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./development/components/js/connect.js":
/*!**********************************************!*\
  !*** ./development/components/js/connect.js ***!
  \**********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _global__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./global */ "./development/components/js/global.js");
// ============================
//    Name: import all modules in one
// ============================
// import './plugins';


/***/ }),

/***/ "./development/components/js/global.js":
/*!*********************************************!*\
  !*** ./development/components/js/global.js ***!
  \*********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! owl.carousel */ "./node_modules/owl.carousel/dist/owl.carousel.js");
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modules/moveScrollUP */ "./development/components/js/modules/moveScrollUP.js");
/* harmony import */ var _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modules/logicCarousel */ "./development/components/js/modules/logicCarousel.js");
/* harmony import */ var _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modules/logicFullImage */ "./development/components/js/modules/logicFullImage.js");
/* harmony import */ var _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_modules_logicFullImage__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _modules_playYoutube__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/playYoutube */ "./development/components/js/modules/playYoutube.js");
/* harmony import */ var _modules_playYoutube__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_modules_playYoutube__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/imageZoomer */ "./development/components/js/modules/imageZoomer.js");
/* harmony import */ var _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_modules_imageZoomer__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/tabSwitcher */ "./development/components/js/modules/tabSwitcher.js");
/* harmony import */ var _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/fetchServerData */ "./development/components/js/modules/fetchServerData.js");
/* harmony import */ var _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_modules_fetchServerData__WEBPACK_IMPORTED_MODULE_7__);
// ============================
//    Name: index.js
// ============================
// import jQuery from 'jquery';







 // import AddFetchCarouselItems from './modules/fetchCarouselItems';
// window.$ = jQuery;
// window.jQuery = jQuery;

var start = function start() {
  console.log('DOM:', 'DOMContentLoaded', true);
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2___default.a('.js__logicCarousel', {
    sm: 1,
    md: 1,
    lg: 1,
    xl: 1,
    xxl: 1
  }, 12, false).run();
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2___default.a('.js__catalogCarousel', {
    sm: 1,
    md: 1,
    lg: 2,
    xl: 5,
    xxl: 6
  }, 0, true).run();
  new _modules_logicCarousel__WEBPACK_IMPORTED_MODULE_2___default.a('.js__historyCarousel', {
    sm: 1,
    md: 1,
    lg: 2,
    xl: 5,
    xxl: 6
  }, 0, true).run();
  new _modules_logicFullImage__WEBPACK_IMPORTED_MODULE_3___default.a('.js__logicCarousel', '.js__imageTakeit').run();
  new _modules_imageZoomer__WEBPACK_IMPORTED_MODULE_5___default.a('.js__imageTakeit', '.js__imageResult').run();
  new _modules_tabSwitcher__WEBPACK_IMPORTED_MODULE_6___default.a('.js__tabsAboutIn', '.js__tabsAboutOut').run();
  new _modules_playYoutube__WEBPACK_IMPORTED_MODULE_4___default.a('.js__playYoutube').run();
  new _modules_moveScrollUP__WEBPACK_IMPORTED_MODULE_1___default.a('.js__moveScrollUP').run();
  new _modules_fetchServerData__WEBPACK_IMPORTED_MODULE_7___default.a('.js__galleryData', {
    server: 'my-server.json',
    selector: 'data-gallery-init'
  }).run(); // new AddFetchCarouselItems('.js__catalogCarousel', {
  // 	server: 'carousel-items.json',
  // 	selector: 'data-gallery-init'
  // }).run();
  // new AddtabSwitcher('.js__tabsAboutIn', '.js__tabsAboutOut').run();
};

if (typeof window !== 'undefined' && window && window.addEventListener) {
  document.addEventListener('DOMContentLoaded', start(), false);
}

/***/ }),

/***/ "./development/components/js/modules/fetchServerData.js":
/*!**************************************************************!*\
  !*** ./development/components/js/modules/fetchServerData.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddFetchServerData(enter, data, exit) {
    _classCallCheck(this, AddFetchServerData);

    this.enter = enter;
    this.exit = exit;
    this.server = data.server;
    this.attr = data.selector;
  }

  _createClass(AddFetchServerData, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.enter)); // const output = document.querySelector(`${this.exit}`);

      if (elem) {
        this.constructor.info();
        var init = {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors',
          cache: 'default'
        };
        fetch("./".concat(this.server), init).then(function (res) {
          res.headers.get('Content-Type');
          return res.json();
        }).then(function (data) {
          var select = document.querySelectorAll("[".concat(_this.attr, "]"));

          var arr = _toConsumableArray(select).map(function (i) {
            return Object.assign(i.dataset);
          });

          var newArr = arr.map(function (x) {
            return x.galleryInit;
          });
          newArr.map(function (i) {
            return document.querySelector("[".concat(_this.attr, "=\"").concat(i, "\"]")).innerText = "".concat(data.one[i] || 'null');
          });
        }).then(function () {
          document.querySelector('.gallery__info').classList.add('no-before');
        }).catch(function (error) {
          console.log("Ouch! Fetch error: \n ".concat(error.message));
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddFetchServerData;
}();

/***/ }),

/***/ "./development/components/js/modules/imageZoomer.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/imageZoomer.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddImageZoomer(enter, exit) {
    _classCallCheck(this, AddImageZoomer);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddImageZoomer, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var result = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var imageZoom = function imageZoom() {
          var lens = document.createElement('DIV');

          if (!elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
            lens.setAttribute('class', 'gallery__lenz');
            elem.parentElement.insertBefore(lens, elem);
            result.style.visibility = 'visible';
          }

          var backgroundPos = function backgroundPos() {
            var cx = result.offsetWidth / lens.offsetWidth;
            var cy = result.offsetHeight / lens.offsetHeight;
            result.style.backgroundImage = "".concat(elem.style.backgroundImage);
            result.style.backgroundSize = "".concat(elem.offsetWidth * cx, "px ").concat(elem.offsetHeight * cy, "px");
          };

          var getCursorPos = function getCursorPos(event) {
            var a = elem.parentElement.getBoundingClientRect();
            var x = 0;
            var y = 0;
            x = event.pageX - a.left;
            y = event.pageY - a.top;
            x -= window.pageXOffset;
            y -= window.pageYOffset;
            return {
              x: x,
              y: y
            };
          };

          var moveLens = function moveLens(event) {
            event.preventDefault();
            var pos = getCursorPos(event);
            var x = pos.x - lens.offsetWidth / 2;
            var y = pos.y - lens.offsetHeight / 2;

            if (x > elem.offsetWidth - lens.offsetWidth) {
              x = elem.offsetWidth - lens.offsetWidth;
            }

            if (x < 0) {
              x = 0;
            }

            if (y > elem.offsetHeight - lens.offsetHeight) {
              y = elem.offsetHeight - lens.offsetHeight;
            }

            if (y < 0) {
              y = 0;
            }

            lens.style.left = "".concat(x, "px");
            lens.style.top = "".concat(y, "px");
            result.style.backgroundPosition = "-".concat(x * result.offsetWidth / lens.offsetWidth, "px -").concat(y * result.offsetHeight / lens.offsetHeight, "px");
          };

          backgroundPos();
          lens.addEventListener('mousemove', moveLens);
          elem.addEventListener('mousemove', moveLens);
          lens.addEventListener('touchmove', moveLens);
          elem.addEventListener('touchmove', moveLens);
        };

        var imageZoomKill = function imageZoomKill(event) {
          if (elem.parentNode.firstElementChild.classList.contains('gallery__lenz')) {
            event.target.firstElementChild.remove();
            result.style.visibility = 'hidden';
          }
        };

        elem.addEventListener('mouseenter', function (event) {
          return imageZoom(event);
        });
        elem.parentElement.addEventListener('mouseleave', function (event) {
          return imageZoomKill(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddImageZoomer;
}();

/***/ }),

/***/ "./development/components/js/modules/logicCarousel.js":
/*!************************************************************!*\
  !*** ./development/components/js/modules/logicCarousel.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddLogicCarousel(enter, init, gap, drag) {
    _classCallCheck(this, AddLogicCarousel);

    this.enter = enter;
    this.drag = drag;
    this.gap = gap;
    this.init = {
      0: {
        items: init.sm
      },
      768: {
        items: init.md
      },
      992: {
        items: init.lg
      },
      1200: {
        items: init.xl
      },
      1600: {
        items: init.xxl
      }
    };
  }

  _createClass(AddLogicCarousel, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.enter)); // const output = document.querySelector(`${this.exit}`);

      if (elem) {
        this.constructor.info();
        var init = {
          loop: true,
          margin: this.gap,
          nav: false,
          dots: false,
          rewind: false,
          touchDrag: this.drag,
          mouseDrag: this.drag,
          merge: false,
          responsive: this.init
        };
        $(this.enter).owlCarousel(init);
        elem.style.visibility = 'visible';

        var control = function control(event, name) {
          event.preventDefault();
          $(_this.enter).trigger("".concat(name, ".owl.carousel"));
        };

        $("".concat(this.enter, "Navigation")).eq(1).on('click', function (event) {
          return control(event, 'next');
        });
        $("".concat(this.enter, "Navigation")).eq(0).on('click', function (event) {
          return control(event, 'prev');
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddLogicCarousel;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/logicFullImage.js":
/*!*************************************************************!*\
  !*** ./development/components/js/modules/logicFullImage.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddLogicFullImage(enter, exit) {
    _classCallCheck(this, AddLogicFullImage);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddLogicFullImage, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var output = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var clickedFunc = function clickedFunc(event) {
          event.preventDefault();
          var curElem = event.target;
          var getData = curElem.src || curElem.firstElementChild.src;
          output.style.backgroundImage = "url(".concat(getData, ")");
        };

        var mainUrl = _toConsumableArray(elem.querySelector('.owl-item').parentElement.children)[4];

        var url = mainUrl.querySelector('img').src;
        output.style.backgroundImage = "url(".concat(url, ")");
        output.style.width = "".concat(output.parentElement.offsetWidth, "px");
        $("".concat(this.enter, " .gallery__item")).on('click', function (event) {
          return clickedFunc(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddLogicFullImage;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/moveScrollUP.js":
/*!***********************************************************!*\
  !*** ./development/components/js/modules/moveScrollUP.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddMoveScrollUP(item, speed) {
    _classCallCheck(this, AddMoveScrollUP);

    this.item = item;
    this.speed = speed;
  }

  _createClass(AddMoveScrollUP, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.item));

      if (elem) {
        this.constructor.info();
        this.speed = this.speed === 'fast' ? 8 / 2 : 8;
        this.speed = this.speed === 'slow' ? 8 * 2 : 8;

        var scrollMe = function scrollMe() {
          var getScroll = document.documentElement.scrollTop || document.body.scrollTop;

          if (getScroll > 0) {
            window.requestAnimationFrame(scrollMe);
            window.scrollTo(0, getScroll - getScroll / _this.speed);
          }
        };

        elem.addEventListener('click', function () {
          return scrollMe();
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddMoveScrollUP;
}();

/***/ }),

/***/ "./development/components/js/modules/playYoutube.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/playYoutube.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function($) {function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddPlayYoutube(item, number, drag) {
    _classCallCheck(this, AddPlayYoutube);

    this.item = item;
    this.number = number;
    this.drag = drag;
  }

  _createClass(AddPlayYoutube, [{
    key: "run",
    value: function run() {
      var _this = this;

      var elem = document.querySelector("".concat(this.item));

      if (elem) {
        this.constructor.info();

        var doit = function doit(event) {
          var qwe = $('#youtube-start-url').attr('data-lazy-src');
          $('#youtube-start-url').attr('src', qwe);
          $("".concat(_this.item)).fadeOut();
        };

        elem.addEventListener('click', function (event) {
          return doit(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddPlayYoutube;
}();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")))

/***/ }),

/***/ "./development/components/js/modules/tabSwitcher.js":
/*!**********************************************************!*\
  !*** ./development/components/js/modules/tabSwitcher.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

module.exports =
/*#__PURE__*/
function () {
  function AddtabSwitcher(enter, exit) {
    _classCallCheck(this, AddtabSwitcher);

    this.enter = enter;
    this.exit = exit;
  }

  _createClass(AddtabSwitcher, [{
    key: "run",
    value: function run() {
      var elem = document.querySelector("".concat(this.enter));
      var output = document.querySelector("".concat(this.exit));

      if (elem) {
        this.constructor.info();

        var logic = function logic(event) {
          event.preventDefault();
          var curElem = event.target;

          var index = _toConsumableArray(curElem.parentElement.children).indexOf(curElem);

          if (output.querySelector('.active')) {
            output.querySelector('.active').classList.remove('active');
          }

          output.children[index].classList.add('active');
        };

        output.firstElementChild.classList.add('active');
        elem.addEventListener('click', function (event) {
          return logic(event);
        });
      }
    }
  }], [{
    key: "info",
    value: function info() {
      console.log('MODULE:', this.name, true);
    }
  }]);

  return AddtabSwitcher;
}();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29ubmVjdC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9jb25uZWN0LmpzIiwid2VicGFjazovLy9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL2dsb2JhbC5qcyIsIndlYnBhY2s6Ly8vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2ZldGNoU2VydmVyRGF0YS5qcyIsIndlYnBhY2s6Ly8vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2ltYWdlWm9vbWVyLmpzIiwid2VicGFjazovLy9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvbG9naWNDYXJvdXNlbC5qcyIsIndlYnBhY2s6Ly8vZGV2ZWxvcG1lbnQvY29tcG9uZW50cy9qcy9tb2R1bGVzL2xvZ2ljRnVsbEltYWdlLmpzIiwid2VicGFjazovLy9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvbW92ZVNjcm9sbFVQLmpzIiwid2VicGFjazovLy9kZXZlbG9wbWVudC9jb21wb25lbnRzL2pzL21vZHVsZXMvcGxheVlvdXR1YmUuanMiLCJ3ZWJwYWNrOi8vL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvbW9kdWxlcy90YWJTd2l0Y2hlci5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImNvbm5lY3RcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL2RldmVsb3BtZW50L2NvbXBvbmVudHMvanMvY29ubmVjdC5qc1wiLFwicGx1Z2luc1wiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsIi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gICAgTmFtZTogaW1wb3J0IGFsbCBtb2R1bGVzIGluIG9uZVxyXG4vLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcblxyXG4vLyBpbXBvcnQgJy4vcGx1Z2lucyc7XHJcbmltcG9ydCAnLi9nbG9iYWwnO1xyXG4iLCIvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09XHJcbi8vICAgIE5hbWU6IGluZGV4LmpzXHJcbi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT1cclxuLy8gaW1wb3J0IGpRdWVyeSBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgJ293bC5jYXJvdXNlbCc7XHJcbmltcG9ydCBBZGRNb3ZlU2Nyb2xsVVAgZnJvbSAnLi9tb2R1bGVzL21vdmVTY3JvbGxVUCc7XHJcbmltcG9ydCBBZGRMb2dpY0Nhcm91c2VsIGZyb20gJy4vbW9kdWxlcy9sb2dpY0Nhcm91c2VsJztcclxuaW1wb3J0IEFkZExvZ2ljRnVsbEltYWdlIGZyb20gJy4vbW9kdWxlcy9sb2dpY0Z1bGxJbWFnZSc7XHJcbmltcG9ydCBBZGRQbGF5WW91dHViZSBmcm9tICcuL21vZHVsZXMvcGxheVlvdXR1YmUnO1xyXG5pbXBvcnQgQWRkSW1hZ2Vab29tZXIgZnJvbSAnLi9tb2R1bGVzL2ltYWdlWm9vbWVyJztcclxuaW1wb3J0IEFkZFRhYlN3aXRjaGVyIGZyb20gJy4vbW9kdWxlcy90YWJTd2l0Y2hlcic7XHJcbmltcG9ydCBBZGRGZXRjaFNlcnZlckRhdGEgZnJvbSAnLi9tb2R1bGVzL2ZldGNoU2VydmVyRGF0YSc7XHJcbi8vIGltcG9ydCBBZGRGZXRjaENhcm91c2VsSXRlbXMgZnJvbSAnLi9tb2R1bGVzL2ZldGNoQ2Fyb3VzZWxJdGVtcyc7XHJcblxyXG4vLyB3aW5kb3cuJCA9IGpRdWVyeTtcclxuLy8gd2luZG93LmpRdWVyeSA9IGpRdWVyeTtcclxuXHJcbmNvbnN0IHN0YXJ0ID0gKCkgPT4ge1xyXG5cdGNvbnNvbGUubG9nKCdET006JywgJ0RPTUNvbnRlbnRMb2FkZWQnLCB0cnVlKTtcclxuXHJcblx0bmV3IEFkZExvZ2ljQ2Fyb3VzZWwoJy5qc19fbG9naWNDYXJvdXNlbCcsIHsgc206IDEsIG1kOiAxLCBsZzogMSwgeGw6IDEsIHh4bDogMSB9LCAxMiwgZmFsc2UpLnJ1bigpO1xyXG5cdG5ldyBBZGRMb2dpY0Nhcm91c2VsKCcuanNfX2NhdGFsb2dDYXJvdXNlbCcsIHsgc206IDEsIG1kOiAxLCBsZzogMiwgeGw6IDUsIHh4bDogNiB9LCAwLCB0cnVlKS5ydW4oKTtcclxuXHRuZXcgQWRkTG9naWNDYXJvdXNlbCgnLmpzX19oaXN0b3J5Q2Fyb3VzZWwnLCB7IHNtOiAxLCBtZDogMSwgbGc6IDIsIHhsOiA1LCB4eGw6IDYgfSwgMCwgdHJ1ZSkucnVuKCk7XHJcblxyXG5cdG5ldyBBZGRMb2dpY0Z1bGxJbWFnZSgnLmpzX19sb2dpY0Nhcm91c2VsJywgJy5qc19faW1hZ2VUYWtlaXQnKS5ydW4oKTtcclxuXHRuZXcgQWRkSW1hZ2Vab29tZXIoJy5qc19faW1hZ2VUYWtlaXQnLCAnLmpzX19pbWFnZVJlc3VsdCcpLnJ1bigpO1xyXG5cdG5ldyBBZGRUYWJTd2l0Y2hlcignLmpzX190YWJzQWJvdXRJbicsICcuanNfX3RhYnNBYm91dE91dCcpLnJ1bigpO1xyXG5cdG5ldyBBZGRQbGF5WW91dHViZSgnLmpzX19wbGF5WW91dHViZScpLnJ1bigpO1xyXG5cdG5ldyBBZGRNb3ZlU2Nyb2xsVVAoJy5qc19fbW92ZVNjcm9sbFVQJykucnVuKCk7XHJcblxyXG5cdG5ldyBBZGRGZXRjaFNlcnZlckRhdGEoJy5qc19fZ2FsbGVyeURhdGEnLCB7XHJcblx0XHRzZXJ2ZXI6ICdteS1zZXJ2ZXIuanNvbicsXHJcblx0XHRzZWxlY3RvcjogJ2RhdGEtZ2FsbGVyeS1pbml0J1xyXG5cdH0pLnJ1bigpO1xyXG5cclxuXHQvLyBuZXcgQWRkRmV0Y2hDYXJvdXNlbEl0ZW1zKCcuanNfX2NhdGFsb2dDYXJvdXNlbCcsIHtcclxuXHQvLyBcdHNlcnZlcjogJ2Nhcm91c2VsLWl0ZW1zLmpzb24nLFxyXG5cdC8vIFx0c2VsZWN0b3I6ICdkYXRhLWdhbGxlcnktaW5pdCdcclxuXHQvLyB9KS5ydW4oKTtcclxuXHJcblx0Ly8gbmV3IEFkZHRhYlN3aXRjaGVyKCcuanNfX3RhYnNBYm91dEluJywgJy5qc19fdGFic0Fib3V0T3V0JykucnVuKCk7XHJcbn07XHJcbmlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIpIHtcclxuXHRkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdET01Db250ZW50TG9hZGVkJywgc3RhcnQoKSwgZmFsc2UpO1xyXG59XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkRmV0Y2hTZXJ2ZXJEYXRhIHtcclxuXHRjb25zdHJ1Y3RvcihlbnRlciwgZGF0YSwgZXhpdCkge1xyXG5cdFx0dGhpcy5lbnRlciA9IGVudGVyO1xyXG5cdFx0dGhpcy5leGl0ID0gZXhpdDtcclxuXHRcdHRoaXMuc2VydmVyID0gZGF0YS5zZXJ2ZXI7XHJcblx0XHR0aGlzLmF0dHIgPSBkYXRhLnNlbGVjdG9yO1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmVudGVyfWApO1xyXG5cdFx0Ly8gY29uc3Qgb3V0cHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmV4aXR9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGluaXQgPSB7XHJcblx0XHRcdFx0bWV0aG9kOiAnR0VUJyxcclxuXHRcdFx0XHRoZWFkZXJzOiB7ICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicgfSxcclxuXHRcdFx0XHRtb2RlOiAnY29ycycsXHJcblx0XHRcdFx0Y2FjaGU6ICdkZWZhdWx0J1xyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0ZmV0Y2goYC4vJHt0aGlzLnNlcnZlcn1gLCBpbml0KVxyXG5cdFx0XHRcdC50aGVuKChyZXMpID0+IHtcclxuXHRcdFx0XHRcdHJlcy5oZWFkZXJzLmdldCgnQ29udGVudC1UeXBlJyk7XHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzLmpzb24oKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC50aGVuKChkYXRhKSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBzZWxlY3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGBbJHt0aGlzLmF0dHJ9XWApO1xyXG5cdFx0XHRcdFx0Y29uc3QgYXJyID0gWy4uLnNlbGVjdF0ubWFwKGkgPT4gKE9iamVjdC5hc3NpZ24oaS5kYXRhc2V0KSkpO1xyXG5cdFx0XHRcdFx0Y29uc3QgbmV3QXJyID0gYXJyLm1hcCh4ID0+IHguZ2FsbGVyeUluaXQpO1xyXG5cdFx0XHRcdFx0bmV3QXJyLm1hcChpID0+IChcclxuXHRcdFx0XHRcdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgWyR7dGhpcy5hdHRyfT1cIiR7aX1cIl1gKS5pbm5lclRleHQgPSBgJHtkYXRhLm9uZVtpXSB8fCAnbnVsbCd9YCkpO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0LnRoZW4oKCkgPT4ge1xyXG5cdFx0XHRcdFx0ZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmdhbGxlcnlfX2luZm8nKS5jbGFzc0xpc3QuYWRkKCduby1iZWZvcmUnKTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdC5jYXRjaCgoZXJyb3IpID0+IHtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGBPdWNoISBGZXRjaCBlcnJvcjogXFxuICR7ZXJyb3IubWVzc2FnZX1gKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkSW1hZ2Vab29tZXIge1xyXG5cdGNvbnN0cnVjdG9yKGVudGVyLCBleGl0KSB7XHJcblx0XHR0aGlzLmVudGVyID0gZW50ZXI7XHJcblx0XHR0aGlzLmV4aXQgPSBleGl0O1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmVudGVyfWApO1xyXG5cdFx0Y29uc3QgcmVzdWx0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmV4aXR9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGltYWdlWm9vbSA9ICgpID0+IHtcclxuXHRcdFx0XHRjb25zdCBsZW5zID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnRElWJyk7XHJcblxyXG5cdFx0XHRcdGlmICghZWxlbS5wYXJlbnROb2RlLmZpcnN0RWxlbWVudENoaWxkLmNsYXNzTGlzdC5jb250YWlucygnZ2FsbGVyeV9fbGVueicpKSB7XHJcblx0XHRcdFx0XHRsZW5zLnNldEF0dHJpYnV0ZSgnY2xhc3MnLCAnZ2FsbGVyeV9fbGVueicpO1xyXG5cdFx0XHRcdFx0ZWxlbS5wYXJlbnRFbGVtZW50Lmluc2VydEJlZm9yZShsZW5zLCBlbGVtKTtcclxuXHRcdFx0XHRcdHJlc3VsdC5zdHlsZS52aXNpYmlsaXR5ID0gJ3Zpc2libGUnO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Y29uc3QgYmFja2dyb3VuZFBvcyA9ICgpID0+IHtcclxuXHRcdFx0XHRcdGNvbnN0IGN4ID0gcmVzdWx0Lm9mZnNldFdpZHRoIC8gbGVucy5vZmZzZXRXaWR0aDtcclxuXHRcdFx0XHRcdGNvbnN0IGN5ID0gcmVzdWx0Lm9mZnNldEhlaWdodCAvIGxlbnMub2Zmc2V0SGVpZ2h0O1xyXG5cdFx0XHRcdFx0cmVzdWx0LnN0eWxlLmJhY2tncm91bmRJbWFnZSA9IGAke2VsZW0uc3R5bGUuYmFja2dyb3VuZEltYWdlfWA7XHJcblx0XHRcdFx0XHRyZXN1bHQuc3R5bGUuYmFja2dyb3VuZFNpemUgPSBgJHsoZWxlbS5vZmZzZXRXaWR0aCAqIGN4KX1weCAkeyhlbGVtLm9mZnNldEhlaWdodCAqIGN5KX1weGA7XHJcblx0XHRcdFx0fTtcclxuXHJcblx0XHRcdFx0Y29uc3QgZ2V0Q3Vyc29yUG9zID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0XHRjb25zdCBhID0gZWxlbS5wYXJlbnRFbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xyXG5cdFx0XHRcdFx0bGV0IHggPSAwO1xyXG5cdFx0XHRcdFx0bGV0IHkgPSAwO1xyXG5cclxuXHRcdFx0XHRcdHggPSBldmVudC5wYWdlWCAtIGEubGVmdDtcclxuXHRcdFx0XHRcdHkgPSBldmVudC5wYWdlWSAtIGEudG9wO1xyXG5cclxuXHRcdFx0XHRcdHggLT0gd2luZG93LnBhZ2VYT2Zmc2V0O1xyXG5cdFx0XHRcdFx0eSAtPSB3aW5kb3cucGFnZVlPZmZzZXQ7XHJcblxyXG5cdFx0XHRcdFx0cmV0dXJuIHsgeCwgeSB9O1xyXG5cdFx0XHRcdH07XHJcblxyXG5cdFx0XHRcdGNvbnN0IG1vdmVMZW5zID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuXHRcdFx0XHRcdGNvbnN0IHBvcyA9IGdldEN1cnNvclBvcyhldmVudCk7XHJcblx0XHRcdFx0XHRsZXQgeCA9IHBvcy54IC0gKGxlbnMub2Zmc2V0V2lkdGggLyAyKTtcclxuXHRcdFx0XHRcdGxldCB5ID0gcG9zLnkgLSAobGVucy5vZmZzZXRIZWlnaHQgLyAyKTtcclxuXHJcblx0XHRcdFx0XHRpZiAoeCA+IGVsZW0ub2Zmc2V0V2lkdGggLSBsZW5zLm9mZnNldFdpZHRoKSB7IHggPSBlbGVtLm9mZnNldFdpZHRoIC0gbGVucy5vZmZzZXRXaWR0aDsgfVxyXG5cdFx0XHRcdFx0aWYgKHggPCAwKSB7IHggPSAwOyB9XHJcblx0XHRcdFx0XHRpZiAoeSA+IGVsZW0ub2Zmc2V0SGVpZ2h0IC0gbGVucy5vZmZzZXRIZWlnaHQpIHsgeSA9IGVsZW0ub2Zmc2V0SGVpZ2h0IC0gbGVucy5vZmZzZXRIZWlnaHQ7IH1cclxuXHRcdFx0XHRcdGlmICh5IDwgMCkgeyB5ID0gMDsgfVxyXG5cclxuXHRcdFx0XHRcdGxlbnMuc3R5bGUubGVmdCA9IGAke3h9cHhgO1xyXG5cdFx0XHRcdFx0bGVucy5zdHlsZS50b3AgPSBgJHt5fXB4YDtcclxuXHJcblx0XHRcdFx0XHRyZXN1bHQuc3R5bGUuYmFja2dyb3VuZFBvc2l0aW9uID0gYC0keyh4ICogcmVzdWx0Lm9mZnNldFdpZHRoIC8gbGVucy5vZmZzZXRXaWR0aCl9cHggLSR7KHkgKiByZXN1bHQub2Zmc2V0SGVpZ2h0IC8gbGVucy5vZmZzZXRIZWlnaHQpfXB4YDtcclxuXHRcdFx0XHR9O1xyXG5cclxuXHRcdFx0XHRiYWNrZ3JvdW5kUG9zKCk7XHJcblx0XHRcdFx0bGVucy5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBtb3ZlTGVucyk7XHJcblx0XHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCBtb3ZlTGVucyk7XHJcblx0XHRcdFx0bGVucy5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBtb3ZlTGVucyk7XHJcblx0XHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCd0b3VjaG1vdmUnLCBtb3ZlTGVucyk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRjb25zdCBpbWFnZVpvb21LaWxsID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0aWYgKGVsZW0ucGFyZW50Tm9kZS5maXJzdEVsZW1lbnRDaGlsZC5jbGFzc0xpc3QuY29udGFpbnMoJ2dhbGxlcnlfX2xlbnonKSkge1xyXG5cdFx0XHRcdFx0ZXZlbnQudGFyZ2V0LmZpcnN0RWxlbWVudENoaWxkLnJlbW92ZSgpO1xyXG5cdFx0XHRcdFx0cmVzdWx0LnN0eWxlLnZpc2liaWxpdHkgPSAnaGlkZGVuJztcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRlbGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlZW50ZXInLCBldmVudCA9PiBpbWFnZVpvb20oZXZlbnQpKTtcclxuXHRcdFx0ZWxlbS5wYXJlbnRFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbGVhdmUnLCBldmVudCA9PiBpbWFnZVpvb21LaWxsKGV2ZW50KSk7XHJcblx0XHR9XHJcblx0fVxyXG59O1xyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IGNsYXNzIEFkZExvZ2ljQ2Fyb3VzZWwge1xyXG5cdGNvbnN0cnVjdG9yKGVudGVyLCBpbml0LCBnYXAsIGRyYWcpIHtcclxuXHRcdHRoaXMuZW50ZXIgPSBlbnRlcjtcclxuXHRcdHRoaXMuZHJhZyA9IGRyYWc7XHJcblx0XHR0aGlzLmdhcCA9IGdhcDtcclxuXHRcdHRoaXMuaW5pdCA9IHtcclxuXHRcdFx0MDogeyBpdGVtczogaW5pdC5zbSB9LFxyXG5cdFx0XHQ3Njg6IHsgaXRlbXM6IGluaXQubWQgfSxcclxuXHRcdFx0OTkyOiB7IGl0ZW1zOiBpbml0LmxnIH0sXHJcblx0XHRcdDEyMDA6IHsgaXRlbXM6IGluaXQueGwgfSxcclxuXHRcdFx0MTYwMDogeyBpdGVtczogaW5pdC54eGwgfVxyXG5cdFx0fTtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3QgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5lbnRlcn1gKTtcclxuXHRcdC8vIGNvbnN0IG91dHB1dCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5leGl0fWApO1xyXG5cdFx0aWYgKGVsZW0pIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdGNvbnN0IGluaXQgPSB7XHJcblx0XHRcdFx0bG9vcDogdHJ1ZSxcclxuXHRcdFx0XHRtYXJnaW46IHRoaXMuZ2FwLFxyXG5cdFx0XHRcdG5hdjogZmFsc2UsXHJcblx0XHRcdFx0ZG90czogZmFsc2UsXHJcblx0XHRcdFx0cmV3aW5kOiBmYWxzZSxcclxuXHRcdFx0XHR0b3VjaERyYWc6IHRoaXMuZHJhZyxcclxuXHRcdFx0XHRtb3VzZURyYWc6IHRoaXMuZHJhZyxcclxuXHRcdFx0XHRtZXJnZTogZmFsc2UsXHJcblx0XHRcdFx0cmVzcG9uc2l2ZTogdGhpcy5pbml0XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHQkKHRoaXMuZW50ZXIpLm93bENhcm91c2VsKGluaXQpO1xyXG5cdFx0XHRlbGVtLnN0eWxlLnZpc2liaWxpdHkgPSAndmlzaWJsZSc7XHJcblxyXG5cdFx0XHRjb25zdCBjb250cm9sID0gKGV2ZW50LCBuYW1lKSA9PiB7XHJcblx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHQkKHRoaXMuZW50ZXIpLnRyaWdnZXIoYCR7bmFtZX0ub3dsLmNhcm91c2VsYCk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHQkKGAke3RoaXMuZW50ZXJ9TmF2aWdhdGlvbmApLmVxKDEpLm9uKCdjbGljaycsIGV2ZW50ID0+IGNvbnRyb2woZXZlbnQsICduZXh0JykpO1xyXG5cdFx0XHQkKGAke3RoaXMuZW50ZXJ9TmF2aWdhdGlvbmApLmVxKDApLm9uKCdjbGljaycsIGV2ZW50ID0+IGNvbnRyb2woZXZlbnQsICdwcmV2JykpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIiwibW9kdWxlLmV4cG9ydHMgPSBjbGFzcyBBZGRMb2dpY0Z1bGxJbWFnZSB7XHJcblx0Y29uc3RydWN0b3IoZW50ZXIsIGV4aXQpIHtcclxuXHRcdHRoaXMuZW50ZXIgPSBlbnRlcjtcclxuXHRcdHRoaXMuZXhpdCA9IGV4aXQ7XHJcblx0fVxyXG5cclxuXHRzdGF0aWMgaW5mbygpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdNT0RVTEU6JywgdGhpcy5uYW1lLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJ1bigpIHtcclxuXHRcdGNvbnN0IGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZW50ZXJ9YCk7XHJcblx0XHRjb25zdCBvdXRwdXQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAke3RoaXMuZXhpdH1gKTtcclxuXHRcdGlmIChlbGVtKSB7XHJcblx0XHRcdHRoaXMuY29uc3RydWN0b3IuaW5mbygpO1xyXG5cclxuXHRcdFx0Y29uc3QgY2xpY2tlZEZ1bmMgPSAoZXZlbnQpID0+IHtcclxuXHRcdFx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cdFx0XHRcdGNvbnN0IGN1ckVsZW0gPSBldmVudC50YXJnZXQ7XHJcblx0XHRcdFx0Y29uc3QgZ2V0RGF0YSA9IGN1ckVsZW0uc3JjIHx8IGN1ckVsZW0uZmlyc3RFbGVtZW50Q2hpbGQuc3JjO1xyXG5cdFx0XHRcdG91dHB1dC5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBgdXJsKCR7Z2V0RGF0YX0pYDtcclxuXHRcdFx0fTtcclxuXHJcblx0XHRcdGNvbnN0IG1haW5VcmwgPSBbLi4uZWxlbS5xdWVyeVNlbGVjdG9yKCcub3dsLWl0ZW0nKS5wYXJlbnRFbGVtZW50LmNoaWxkcmVuXVs0XTtcclxuXHRcdFx0Y29uc3QgdXJsID0gbWFpblVybC5xdWVyeVNlbGVjdG9yKCdpbWcnKS5zcmM7XHJcblx0XHRcdG91dHB1dC5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBgdXJsKCR7dXJsfSlgO1xyXG5cdFx0XHRvdXRwdXQuc3R5bGUud2lkdGggPSBgJHtvdXRwdXQucGFyZW50RWxlbWVudC5vZmZzZXRXaWR0aH1weGA7XHJcblxyXG5cdFx0XHQkKGAke3RoaXMuZW50ZXJ9IC5nYWxsZXJ5X19pdGVtYCkub24oJ2NsaWNrJywgZXZlbnQgPT4gY2xpY2tlZEZ1bmMoZXZlbnQpKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkTW92ZVNjcm9sbFVQIHtcclxuXHRjb25zdHJ1Y3RvcihpdGVtLCBzcGVlZCkge1xyXG5cdFx0dGhpcy5pdGVtID0gaXRlbTtcclxuXHRcdHRoaXMuc3BlZWQgPSBzcGVlZDtcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3QgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5pdGVtfWApO1xyXG5cdFx0aWYgKGVsZW0pIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdHRoaXMuc3BlZWQgPSB0aGlzLnNwZWVkID09PSAnZmFzdCcgPyA4IC8gMiA6IDg7XHJcblx0XHRcdHRoaXMuc3BlZWQgPSB0aGlzLnNwZWVkID09PSAnc2xvdycgPyA4ICogMiA6IDg7XHJcblxyXG5cdFx0XHRjb25zdCBzY3JvbGxNZSA9ICgpID0+IHtcclxuXHRcdFx0XHRjb25zdCBnZXRTY3JvbGwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wIHx8IGRvY3VtZW50LmJvZHkuc2Nyb2xsVG9wO1xyXG5cdFx0XHRcdGlmIChnZXRTY3JvbGwgPiAwKSB7XHJcblx0XHRcdFx0XHR3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHNjcm9sbE1lKTtcclxuXHRcdFx0XHRcdHdpbmRvdy5zY3JvbGxUbygwLCBnZXRTY3JvbGwgLSBnZXRTY3JvbGwgLyB0aGlzLnNwZWVkKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH07XHJcblx0XHRcdGVsZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiBzY3JvbGxNZSgpKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkUGxheVlvdXR1YmUge1xyXG5cdGNvbnN0cnVjdG9yKGl0ZW0sIG51bWJlciwgZHJhZykge1xyXG5cdFx0dGhpcy5pdGVtID0gaXRlbTtcclxuXHRcdHRoaXMubnVtYmVyID0gbnVtYmVyO1xyXG5cdFx0dGhpcy5kcmFnID0gZHJhZztcclxuXHR9XHJcblxyXG5cdHN0YXRpYyBpbmZvKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ01PRFVMRTonLCB0aGlzLm5hbWUsIHRydWUpO1xyXG5cdH1cclxuXHJcblx0cnVuKCkge1xyXG5cdFx0Y29uc3QgZWxlbSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYCR7dGhpcy5pdGVtfWApO1xyXG5cdFx0aWYgKGVsZW0pIHtcclxuXHRcdFx0dGhpcy5jb25zdHJ1Y3Rvci5pbmZvKCk7XHJcblx0XHRcdFxyXG5cdFx0XHRjb25zdCBkb2l0ID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0Y29uc3QgcXdlID0gJCgnI3lvdXR1YmUtc3RhcnQtdXJsJykuYXR0cignZGF0YS1sYXp5LXNyYycpO1xyXG5cdFx0XHRcdCQoJyN5b3V0dWJlLXN0YXJ0LXVybCcpLmF0dHIoJ3NyYycsIHF3ZSk7XHJcblx0XHRcdFx0JChgJHt0aGlzLml0ZW19YCkuZmFkZU91dCgpO1xyXG5cdFx0XHR9O1xyXG5cdFx0XHRcclxuXHRcdFx0ZWxlbS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGV2ZW50ID0+IGRvaXQoZXZlbnQpKTtcclxuXHRcdH1cclxuXHR9XHJcbn07XHJcbiIsIm1vZHVsZS5leHBvcnRzID0gY2xhc3MgQWRkdGFiU3dpdGNoZXIge1xyXG5cdGNvbnN0cnVjdG9yKGVudGVyLCBleGl0KSB7XHJcblx0XHR0aGlzLmVudGVyID0gZW50ZXI7XHJcblx0XHR0aGlzLmV4aXQgPSBleGl0O1xyXG5cdH1cclxuXHJcblx0c3RhdGljIGluZm8oKSB7XHJcblx0XHRjb25zb2xlLmxvZygnTU9EVUxFOicsIHRoaXMubmFtZSwgdHJ1ZSk7XHJcblx0fVxyXG5cclxuXHRydW4oKSB7XHJcblx0XHRjb25zdCBlbGVtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmVudGVyfWApO1xyXG5cdFx0Y29uc3Qgb3V0cHV0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgJHt0aGlzLmV4aXR9YCk7XHJcblx0XHRpZiAoZWxlbSkge1xyXG5cdFx0XHR0aGlzLmNvbnN0cnVjdG9yLmluZm8oKTtcclxuXHJcblx0XHRcdGNvbnN0IGxvZ2ljID0gKGV2ZW50KSA9PiB7XHJcblx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHRcdFx0XHRjb25zdCBjdXJFbGVtID0gZXZlbnQudGFyZ2V0O1xyXG5cdFx0XHRcdGNvbnN0IGluZGV4ID0gWy4uLmN1ckVsZW0ucGFyZW50RWxlbWVudC5jaGlsZHJlbl0uaW5kZXhPZihjdXJFbGVtKTtcclxuXHJcblx0XHRcdFx0aWYgKG91dHB1dC5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJykpIHtcclxuXHRcdFx0XHRcdG91dHB1dC5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlJykuY2xhc3NMaXN0LnJlbW92ZSgnYWN0aXZlJyk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdG91dHB1dC5jaGlsZHJlbltpbmRleF0uY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdH07XHJcblxyXG5cdFx0XHRvdXRwdXQuZmlyc3RFbGVtZW50Q2hpbGQuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJyk7XHJcblx0XHRcdGVsZW0uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBldmVudCA9PiBsb2dpYyhldmVudCkpO1xyXG5cdFx0fVxyXG5cdH1cclxufTtcclxuIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN0SkE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUVBOzs7Ozs7Ozs7Ozs7O0FDSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRkE7QUFNQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1Q0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUEE7QUFBQTtBQUFBO0FBWUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFKQTtBQU9BO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBRUE7QUFFQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUE1Q0E7QUFBQTtBQUFBO0FBU0E7QUFDQTtBQVZBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFqRkE7QUFBQTtBQUFBO0FBT0E7QUFDQTtBQVJBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBTEE7QUFPQTtBQUNBO0FBYkE7QUFBQTtBQUFBO0FBa0JBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVEE7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlDQTtBQUFBO0FBQUE7QUFlQTtBQUNBO0FBaEJBO0FBQ0E7QUFEQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBV0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBRUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBTEE7QUFBQTtBQUFBO0FBVUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTFCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQU5BO0FBQUE7QUFBQTtBQVdBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQXhCQTtBQUFBO0FBQUE7QUFRQTtBQUNBO0FBVEE7QUFDQTtBQURBO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7QUFBQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFMQTtBQUFBO0FBQUE7QUFXQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQTlCQTtBQUFBO0FBQUE7QUFPQTtBQUNBO0FBUkE7QUFDQTtBQURBO0FBQUE7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==